import Vue from 'vue/dist/vue.min.js'
import Router from 'vue-router';
window.Vue = require('vue');
Vue.use(Router);

import products from './components/views/Products.vue'
import home from './components/views/Home.vue'


export default  new Router({
    routes:[
        {
            path: '/home',
            name: 'home',
            component: home,
        },
        {
            path: '/products',
            name: 'products',
            component: products,
        }
    ],
    mode: 'history'

})