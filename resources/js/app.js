require('./bootstrap');
import router from './routes';
import VueRouter from 'vue-router'
import Vue from 'vue'
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
window.Vue = require('vue').default;

Vue.router = router
Vue.use(VueRouter)
Vue.use(VueSweetalert2)



Vue.component('dashboard', require('./components/Dashboard.vue').default);
Vue.component('home', require('./components/views/Home.vue').default);
Vue.component('products', require('./components/views/Products.vue').default);



const app = new Vue({
    el: '#app',
    router,
});
