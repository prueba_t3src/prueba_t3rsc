<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Prueba t3rsc</title>

        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{!! asset('css/bootstrap.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/bootstrap-extend.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/fonts/master_style.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/_all-skins.css') !!}">	
        <link rel="stylesheet" href="{!! asset('css/morris.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/fonts/weather-icons.css') !!}">
        <script src="https://kit.fontawesome.com/ab7c81d8d6.js" crossorigin="anonymous"></script>

    </head>
    <body class="antialiased hold-transition skin-purple-light sidebar-mini">
        <div id="app">
            <dashboard></dashboard>
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="{!! asset('js/jquery-3.3.1.js') !!}"></script>
        <script src="{!! asset('js/popper.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap.js') !!}"></script>	
        <script src="{!! asset('js/jquery.slimscroll.js') !!}"></script>
        <script src="{!! asset('js/fastclick.js') !!}"></script>
        <script src="{!! asset('js/jquery.sparkline.min.js') !!}"></script>
        <script src="{!! asset('js/raphael.min.js') !!}"></script>
        <script src="{!! asset('js/morris.min.js') !!}"></script>	
        <script src="{!! asset('js/Chart.bundle.js') !!}"></script>
        <script src="{!! asset('js/jquery.flot.js') !!}"></script>
        <script src="{!! asset('js/jquery.flot.resize.js') !!}"></script>
        <script src="{!! asset('js/WeatherIcon.js') !!}"></script>
        <script src="{!! asset('js/template.js') !!}"></script>
        <script src="{!! asset('js/dashboard.js') !!}"></script>
        <script src="{!! asset('js/demo.js') !!}"></script>
        <script src="{!! asset('js/statistic.js') !!}"></script>
    </body>
</html>
