<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Product;

class ProductController extends Controller
{
    public function getBrands(){
        $brands = Brand::all();
        return $brands;
    }

    public function getProducts(){
        $products = Product::where('status', 'ACTIVE')
        ->join('brands', 'brands.id', 'products.brand_id')
        ->select('products.*', 'brands.name as nameBrand')->get();
        return response()->json($products);
    }

    public function store(Request $request){
        Product::create([
            'brand_id' => $request->brand_id,
            'name' => $request->name,
            'description' => $request->description,
            'cost' => $request->cost,
            'status' => 'ACTIVE'
        ]);
        return $message = 'Success';
    }

    public function update(Request $request){
        Product::find($request->idProduct)
        ->update([
            'brand_id' => $request->brand_id,
            'name' => $request->name,
            'description' => $request->description,
            'cost' => $request->cost
        ]);
        return $message = 'Success';
    }

    public function changeStatus($id){
        Product::find($id)
        ->update([
            'status' => "INACTIVE",
        ]);
        return $message = 'Success';

    }
}
