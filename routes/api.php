<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProductController;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('cliente/create', [ClienteController::class, 'store']);
Route::post('getClientes', [ClienteController::class, 'getClientes']);
Route::post('editCliente/{id}', [ClienteController::class, 'edit']);
Route::put('cliente/update', [ClienteController::class, 'update']);
Route::delete('cliente/changeStatus/{id}', [ClienteController::class, 'changeStatus']);

Route::post('getBrands', [ProductController::class, 'getBrands']);
Route::post('getProducts', [ProductController::class, 'getProducts']);
Route::post('product/store', [ProductController::class, 'store']);
Route::put('product/update', [ProductController::class, 'update']);
Route::delete('product/changeStatus/{id}', [ProductController::class, 'changeStatus']);
